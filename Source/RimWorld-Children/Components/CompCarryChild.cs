﻿namespace RimWorldChildren {
    using RimWorld;
    using RimWorldChildren.Api;
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using Verse;

    /// <summary>
    /// This comp handles the rendering of a carried pawn over top of the carrying pawn.
    /// </summary>
    public class CompCarryChild : ThingComp {

        public static List<PawnCapacityModifier> capMods = new List<PawnCapacityModifier>();
        public Pawn Carrier;
        public Pawn Carried;

        /// <summary>
        /// Initializes static members of the <see cref="CompCarryChild"/> class.
        /// Setup a static capMod collection to be applied to any pawns carrying children.
        /// </summary>
        static CompCarryChild() {
            PawnCapacityModifier mod = new PawnCapacityModifier();
            mod.capacity = PawnCapacityDefOf.Manipulation;
            mod.postFactor = 0.5f;
            capMods.Add(mod);
        }

        public CompProperties_CarryChild Props {
            get => (CompProperties_CarryChild)props;
        }

        /// <summary>
        /// A flag indicating a mod has requested to terminate the carry
        /// </summary>
        public bool ShouldDrop { get; private set; }

        public override void PostExposeData() {
            Scribe_References.Look(ref Carrier, "carrier");
            Scribe_References.Look(ref Carried, "carried");
        }

        public override void PostDraw() {
            Pawn pawn = parent as Pawn;
            if (CarryUtility.IsCarried(pawn)) {
                Vector3 vector = CarryUtility.AdjustCarryLayer(pawn.DrawPos, pawn);

                // Use Drawer instead of Thing.DrawAt to prevent a call back loop thru PostDraw.
                pawn.Drawer.DrawAt(vector);
            } 
        }

        /// <summary>
        /// This method will forcefully end the carrying of a child if executed on either the carrier or the carried.
        /// </summary>
        public void ForceDrop() {
            ShouldDrop = true;
        }

        /// <summary>
        /// Fully resets the state of this component.
        /// </summary>
        public void Reset() {
            Carried = null;
            Carrier = null;
            ShouldDrop = false;
        }
    }

    public class CompProperties_CarryChild : CompProperties {

        public CompProperties_CarryChild() {
            compClass = typeof(CompCarryChild);
        }

        public CompProperties_CarryChild(Type compClass) : base(compClass) {
            this.compClass = compClass;
        }
    }
}
