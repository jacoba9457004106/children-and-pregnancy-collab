﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using Verse;

    /// <summary>
    /// This def will cover all race specific configurations for childhood, birth, and pregnancy.
    /// </summary>
    public class RacialLifestageDef : Def {
        public string raceDefName;
        public List<LifecycleStage> lifeStages;

        // Required: Base percentage chance that an impregnation attempt will be successful
        public float impregnationChance;

        // Required: Represents the number of days a pregnancy with this race lasts
        public int gestationDays;

        // Optional: If false, this pawn will be able to become pregnant immediately after birth
        public bool postpartumEnabled = true;
    }
}
