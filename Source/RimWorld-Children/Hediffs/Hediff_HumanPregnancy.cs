﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using System.Linq;
    using RimWorld;
    using RimWorld.Planet;
    using RimWorldChildren.API;
    using UnityEngine;
    using Verse;
    using Verse.Sound;

    [AssociatedHediff("HumanPregnancy")]
    public class Hediff_HumanPregnancy : Hediff_Pregnancy {
        internal static void GiveRandomBirthDefect(Pawn baby) {
            int r = Rand.Range(1, 9);
            CLog.Message("Adding Birth Defect: " + r);

            // Bad back defect
            if (r == 1) {
                baby.health.AddHediff(HediffDefOf.BadBack, ChildrenUtility.GetPawnBodyPart(baby, "Spine"), null);
            }

            // cataract defect
            if (r == 2) {
                baby.health.AddHediff(HediffDefOf.Cataract, ChildrenUtility.GetPawnBodyPart(baby, "Eye"), null);
            }

            // frail defect
            if (r == 3) {
                baby.health.AddHediff(HediffDefOf.Frail, null, null);
            }

            // deafness
            if (r == 4) {
                foreach (BodyPartRecord record in ChildrenUtility.GetPawnBodyParts(baby, "Ear")){
                    baby.health.AddHediff(HediffDef.Named("HearingLoss"), record, null);
                }
            }

            // blindness
            if (r == 5) {
                foreach (BodyPartRecord record in ChildrenUtility.GetPawnBodyParts(baby, "Eye")) {
                    baby.health.AddHediff(HediffDefOf.Blindness, record, null);
                }
            }

            // heart defect
            if (r == 6) {
                baby.health.AddHediff(HediffDef.Named("DefectHeart"), ChildrenUtility.GetPawnBodyPart(baby, "Heart"), null);
            }

            // stillbirth
            if (r >= 7) {
                baby.health.AddHediff(HediffDef.Named("DefectStillborn"), null, null);
            }
        }

        protected override void PostBirth() {
            pawn.health.AddHediff(HediffDef.Named("PostPregnancy"), null, null);
            pawn.health.AddHediff(HediffDef.Named("Lactating"), ChildrenUtility.GetPawnBodyPart(pawn, "Torso"), null);
            if (pawn.TryGetComp<LifecycleComp>().LifecycleDef.postpartumEnabled) {
                pawn.health.AddHediff(HediffDef.Named("PostPartum"), null, null);
            }

            if (successfulBirth == true) {
                // Give father a happy memory if the birth was successful and he's not dead
                if (father != null && !father.health.Dead) {
                    father.needs.mood.thoughts.memories.TryGainMemory(ThoughtDef.Named("PartnerGaveBirth"));
                    RemovePregnancyMemories(father);
                    pawn.needs.mood.thoughts.memories.TryGainMemory(ThoughtDef.Named("WeHadBabies"), father);
                    father.needs.mood.thoughts.memories.TryGainMemory(ThoughtDef.Named("WeHadBabies"), pawn);
                }

                RemovePregnancyMemories(pawn);
                pawn.needs.mood.thoughts.memories.RemoveMemoriesOfDef(ThoughtDef.Named("IAmPregnant"));

                // Try to give PPD. If not, give "New baby" thought
                float chance = 0.2f;
                if (pawn.story.traits.HasTrait(TraitDefOf.Psychopath)) {
                    chance -= 1;
                }

                if (pawn.story.traits.HasTrait(TraitDef.Named("Nerves"))) {
                    chance -= 0.2f * pawn.story.traits.GetTrait(TraitDef.Named("Nerves")).Degree;
                }
                else if (pawn.story.traits.HasTrait(TraitDef.Named("NaturalMood"))) {
                    if (pawn.story.traits.GetTrait(TraitDef.Named("NaturalMood")).Degree == 2) {
                        chance -= 1;
                    }

                    if (pawn.story.traits.GetTrait(TraitDef.Named("NaturalMood")).Degree == -2) {
                        chance += 0.6f;
                    }
                }

                chance = Mathf.Clamp(chance, 0, 1);
                CLog.Message("Chance of PPD is " + (chance * 100) + "%");

                // Try to give PPD
                if (Rand.Value < chance) {
                    pawn.needs.mood.thoughts.memories.TryGainMemory(ThoughtDef.Named("PostPartumDepression"), null);
                    bool verybad = false;
                    if (pawn.story.traits.HasTrait(TraitDef.Named("NaturalMood"))) {
                        if (pawn.story.traits.GetTrait(TraitDef.Named("NaturalMood")).Degree == -2) {
                            verybad = true;
                        }
                    }
                    else if (pawn.story.traits.HasTrait(TraitDef.Named("Neurotic"))) {
                        if (pawn.story.traits.GetTrait(TraitDef.Named("Neurotic")).Degree == 2) {
                            verybad = true;
                        }
                    }

                    // This pawn gets an exceptionally bad case of PPD
                    if (verybad) {
                        foreach (Thought_Memory thought in pawn.needs.mood.thoughts.memories.Memories) {
                            if (thought.def.defName == "PostPartumDepression") {
                                thought.SetForcedStage(thought.CurStageIndex + 1);
                            }
                        }
                    }
                }
                else {
                    // If we didn't get PPD, then the pawn gets a mood buff
                    if (pawn.relations.ChildrenCount == babies.Count) {
                        pawn.needs.mood.thoughts.memories.TryGainMemory(ThoughtDef.Named("IGaveBirthFirstTime"));
                    }
                    else {
                        pawn.needs.mood.thoughts.memories.TryGainMemory(ThoughtDef.Named("IGaveBirth"));
                    }
                }

                float birthing_quality = pawn.health.hediffSet.GetFirstHediffOfDef(HediffDef.Named("GivingBirth")).TryGetComp<HediffComp_TendDuration>().tendQuality;
                if (pawn.Spawned) {
                    // Spawn guck
                    FilthMaker.TryMakeFilth(pawn.Position, pawn.Map, ThingDefOf.Filth_AmnioticFluid, pawn.LabelIndefinite(), 5);
                    pawn?.caller?.DoCall();

                    Log.Message("Birth quality = " + birthing_quality);

                    // Possible tearing from pregnancy
                    if (birthing_quality < 0.75f) {
                        if (birthing_quality < Rand.Value) {
                            // Add a tear from giving birth
                            if (birthing_quality < Rand.Value) {
                                pawn.health.AddHediff(HediffDef.Named("PregnancyTearMajor"), ChildrenUtility.GetPawnBodyPart(pawn, "Torso"), null);
                            }
                            else {
                                pawn.health.AddHediff(HediffDef.Named("PregnancyTear"), ChildrenUtility.GetPawnBodyPart(pawn, "Torso"), null);
                            }
                        }
                    }

                    // If the mother does not belong to the player, is not a guest or prisoner, and is downed after the birth
                    // Attempt to have another pawn from her faction rescue her
                    if (pawn.Downed && !(pawn.guest.HostFaction == Faction.OfPlayer) && pawn.Faction != Faction.OfPlayer) {
                        CLog.Message("Attempting to carry mother off map");
                        ChildrenUtility.CarryFactionPawnToExit(pawn);
                    }
                }

                // All other colonists hear the good news
                foreach (Pawn c in PawnsFinder.AllMapsCaravansAndTravelingTransportPods_Alive_FreeColonists_NoCryptosleep) {
                    if (c != pawn && c != father && ChildrenUtility.GetLifestageType(c) > LifestageType.Toddler) {
                        c.needs.mood.thoughts.memories.TryGainMemory(ThoughtDef.Named("ColonyBirth"), null);
                    }
                }
            }
        }

        protected override void PostBirth(Pawn baby) {
            base.PostBirth(baby);

            // setup relations
            if (baby.RaceProps.IsFlesh) {
                baby.relations.AddDirectRelation(PawnRelationDefOf.Parent, pawn);
                if (father != null) {
                    baby.relations.AddDirectRelation(PawnRelationDefOf.Parent, father);
                }
            }

            List<Pawn> maybeSiblings = babies.Where(bby => bby != baby).ToList();
            if (maybeSiblings != null) {
                foreach (Pawn sibling in maybeSiblings) {
                    baby.relations.AddDirectRelation(PawnRelationDefOf.Sibling, sibling);
                }
            }

            if (successfulBirth) {
                // Send a message that the baby was born
                if (pawn.health.hediffSet.GetFirstHediffOfDef(HediffDef.Named("HumanPregnancy")).Visible && PawnUtility.ShouldSendNotificationAbout(pawn)) {
                    if (!pawn.IsPrisonerOfColony) {
                        Find.LetterStack.ReceiveLetter("LabelGaveBirth".Translate(baby.LabelIndefinite()),
                            "MessageHumanBirth".Translate(pawn.LabelIndefinite(), baby.Name.ToStringShort),
                            LetterDefOf.PositiveEvent, baby, null);
                    } else {
                        Find.LetterStack.ReceiveLetter("LabelPrisonerBirth".Translate(baby.LabelIndefinite()),
                            "MessagePrisonerBirth".Translate(pawn.LabelIndefinite()),
                            LetterDefOf.PositiveEvent, baby, null);
                    }
                }

                // Make crying sound when baby is born
                SoundInfo info = SoundInfo.InMap(new TargetInfo(baby.PositionHeld, baby.MapHeld));
                SoundDef.Named("Pawn_BabyCry").PlayOneShot(info);

                // Give baby newborn thought
                baby.needs.mood.thoughts.memories.TryGainMemory(ThoughtDef.Named("JustBorn"), null);
            }

            // If baby isn't a colonist or guest/prisoner, have someone try to take them off map
            if (!(baby.guest.HostFaction == Faction.OfPlayer) && baby.Faction != Faction.OfPlayer) {
                CLog.Message("Attempting to carry baby off map");
                ChildrenUtility.CarryFactionPawnToExit(baby);
            }

            baby?.caller?.DoCall();
        }

        protected override void Birth(Pawn baby, float chance_successful = 1.0f) {
            CLog.Message(pawn.Name + " is giving birth");
            if (father == null) {
                CLog.Warning("No father defined");
            }

            if (PawnUtility.TrySpawnHatchedOrBornPawn (baby, pawn)) {
                if (baby.playerSettings != null && pawn.playerSettings != null) {
                    baby.playerSettings.AreaRestriction = pawn.playerSettings.AreaRestriction;
                }

                // Move the baby in front of the mother, rather than on top
                if (pawn.CurrentBed() != null) {
                    baby.Position = baby.Position + new IntVec3(0, 0, 1).RotatedBy(pawn.CurrentBed().Rotation);
                }

                // The baby died from bad chance of success
                if (Rand.Value > chance_successful || chance_successful <= 0) {
                    successfulBirth = false;
                }

                // Birth defects via drugs or alcohol
                if (pawn.health.hediffSet.HasHediff(HediffDef.Named("BirthDefectTracker"))) {
                    Hediff_BirthDefectTracker tracker = (Hediff_BirthDefectTracker)pawn.health.hediffSet.GetFirstHediffOfDef(HediffDef.Named("BirthDefectTracker"));

                    // The baby died in utero from chemical affect
                    if (tracker.stillbirth) {
                        successfulBirth = false;
                    }

                    // The baby lived! So far, anyways
                    else {
                        // Should the baby get fetal alcohol syndrome?
                        if (tracker.fetal_alcohol) {
                            baby.health.AddHediff(HediffDef.Named("FetalAlcoholSyndrome"));
                        }

                        // If the mother got high while pregnant, baby is addicted
                        if (tracker.drug_addictions.Count > 0) {
                            foreach (HediffDef addiction in tracker.drug_addictions) {
                                baby.health.AddHediff(addiction, null, null);
                            }
                        }
                    }
                }

                // Inbred?
                if (father != null && pawn.relations.FamilyByBlood.Contains(father)) {
                    // 50% chance to get a birth defect from inbreeding
                    if (Rand.Bool) {
                        GiveRandomBirthDefect (baby);
                        if (baby.health.hediffSet.HasHediff(HediffDef.Named("DefectStillborn"))) {
                            successfulBirth = false;
                        }
                    }
                }

                if (!successfulBirth) {
                    Miscarry(baby);
                }
            }
            else {
                Find.WorldPawns.PassToWorld(baby, PawnDiscardDecideMode.Discard);
            }
        }

        public override void Miscarry (Pawn baby) {
            if (baby != null) {
                baby.Name = new NameSingle ("Unnamed", false);
                baby.SetFaction (null, null);
                baby.health.AddHediff (HediffDef.Named ("DefectStillborn"));
            }

            if (IsLateTerm()) {
                if (father != null) {
                    father.needs.mood.thoughts.memories.TryGainMemory(ThoughtDef.Named("BabyStillborn"), baby);
                }

                pawn.needs.mood.thoughts.memories.TryGainMemory(ThoughtDef.Named("BabyStillborn"), baby);
            }

            base.Miscarry(baby);
        }

        public override void Abort(Pawn baby) {
            if (IsLateTerm()) {
                pawn.needs.mood.thoughts.memories.TryGainMemory(ThoughtDef.Named("LateTermAbortion"), null);
                if (GestationProgress >= 0.90f) {
                    Thought_Memory abortion_thought = pawn.needs.mood.thoughts.memories.OldestMemoryOfDef(ThoughtDef.Named("LateTermAbortion"));

                    // Very late term abortion
                    abortion_thought.SetForcedStage(abortion_thought.CurStageIndex + 1);
                }
            }

            if (baby != null) {
                baby.Name = new NameSingle("Unnamed", false);
                baby.SetFaction(null, null);
            }

            base.Abort(baby);
        }

        public override void PostDiscoverPregnancy() {
            if (father != null && !father.health.Dead) {
                // father is happy that partner is pregnant
                father.needs.mood.thoughts.memories.TryGainMemory(ThoughtDef.Named("PartnerIsPregnant"));
                father.needs.mood.thoughts.memories.TryGainMemory(ThoughtDef.Named("WeGotPregnant"), pawn);
                pawn.needs.mood.thoughts.memories.TryGainMemory(ThoughtDef.Named("WeGotPregnant"), father);
            }

            pawn.needs.mood.thoughts.memories.TryGainMemory(ThoughtDef.Named("IAmPregnant"));
        }

        private void RemovePregnancyMemories(Pawn pawn) {
            pawn.needs.mood.thoughts.memories.RemoveMemoriesOfDef(ThoughtDef.Named("IAmPregnant"));
            pawn.needs.mood.thoughts.memories.RemoveMemoriesOfDef(ThoughtDef.Named("PartnerIsPregnant"));
            pawn.needs.mood.thoughts.memories.RemoveMemoriesOfDef(ThoughtDef.Named("WeGotPregnant"));
        }

        protected override void PostAbort(Pawn baby) {
            RemovePregnancyMemories(pawn);

            if (father != null && !father.health.Dead) {
                RemovePregnancyMemories(father);
            }
        }

        protected override void PostMiscarry(Pawn baby) {
            RemovePregnancyMemories(pawn);
            if (father != null && !father.health.Dead) {
                RemovePregnancyMemories(father);
            }
        }
    }
}