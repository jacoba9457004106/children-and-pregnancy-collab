﻿namespace RimWorldChildren {
    using RimWorld;
    using RimWorldChildren.API;
    using Verse;

    public class InteractionWorker_BabyGames : InteractionWorker {
        public override float RandomSelectionWeight(Pawn initiator, Pawn recipient) {
            if (!ChildrenUtility.RaceUsesChildren(recipient)) {
                return 0;
            }

            // Toddlers can play babygames with each other casually, otherwise interaction can only be done with baby
            if (ChildrenUtility.GetLifestageType(initiator) == LifestageType.Toddler && ChildrenUtility.GetLifestageType(recipient) == LifestageType.Toddler) {
                return 1f;
            }

            if (ChildrenUtility.GetLifestageType(recipient) > LifestageType.Baby) {
                return 0;
            }

            return 1f;
        }
    }
}
