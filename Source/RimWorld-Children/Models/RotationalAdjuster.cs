﻿using UnityEngine;

namespace RimWorldChildren {
    /// <summary>
    /// Defines rules for adjusting graphics based on rotation
    /// </summary>
    public class RotationalAdjuster {
        // Required: Part offset.
        public Vector2 offset = Vector2.zero;

        // Required: Part scaling.
        public Vector2 scale = Vector2.zero;
    }
}
