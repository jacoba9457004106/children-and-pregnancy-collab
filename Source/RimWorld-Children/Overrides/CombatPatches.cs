﻿namespace RimWorldChildren {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    using HarmonyLib;
    using RimWorld;
    using RimWorldChildren.API;
    using Verse;
    using Verse.AI;

    /// <summary>
    /// Harmony patches related to weapons, combat, etc
    /// </summary>
    public class CombatPatches {
        /// <summary>
        /// Causes children to drop too-heavy weapons and potentially hurt themselves on firing
        /// </summary>
        [HarmonyPatch(typeof(Verb_Shoot), "TryCastShot")]
        public static class VerbShoot_TryCastShot_Patch {
            [HarmonyPostfix]
            internal static void TryCastShot_Patch(ref Verb_Shoot __instance) {
                Pawn pawn = __instance.CasterPawn;
                if (pawn != null && ChildrenUtility.RaceUsesChildren(pawn) && ChildrenUtility.GetLifestageType(pawn) <= LifestageType.Child) {
                    // The weapon is too heavy and the child will (likely) drop it when trying to fire
                    if (__instance.EquipmentSource.def.BaseMass > ChildrenUtility.ChildMaxWeaponMass(pawn)) {
                        ThingWithComps benis;
                        pawn.equipment.TryDropEquipment(__instance.EquipmentSource, out benis, pawn.Position, false);

                        float recoilForce = __instance.EquipmentSource.def.BaseMass - 3;

                        if (recoilForce > 0) {
                            string[] hitPart = {
                            "Torso",
                            "Shoulder",
                            "Arm",
                            "Hand",
                            "Head",
                            "Neck",
                            "Eye",
                            "Nose",
                        };
                            int hits = Rand.Range(1, 4);
                            while (hits > 0) {
                                pawn.TakeDamage(new DamageInfo(DamageDefOf.Blunt, (int)((recoilForce + Rand.Range(0f, 3f)) / hits), 0, -1, __instance.EquipmentSource, ChildrenUtility.GetPawnBodyPart(pawn, hitPart.RandomElement()), null));
                                hits--;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gives a notification that the weapon a child has picked up is dangerous for them to handle
        /// </summary>
        [HarmonyPatch(typeof(Pawn_EquipmentTracker), "Notify_EquipmentAdded")]
        public static class PawnEquipmentracker_NotifyEquipmentAdded_Patch {
            [HarmonyPostfix]
            internal static void Notify_EquipmentAdded_Patch(ref ThingWithComps eq, ref Pawn_EquipmentTracker __instance) {
                Pawn pawn = __instance.ParentHolder as Pawn;
                if (pawn != null && ChildrenUtility.RaceUsesChildren(pawn) && eq.def.BaseMass > ChildrenUtility.ChildMaxWeaponMass(pawn) && ChildrenUtility.GetLifestageType(pawn) <= LifestageType.Child && pawn.Faction.IsPlayer) {
                    Messages.Message("MessageWeaponTooLarge".Translate(eq.def.label, ((Pawn)__instance.ParentHolder).Name.ToStringShort), MessageTypeDefOf.CautionInput);
                }
            }
        }

        /// <summary>
        /// Prevents children from being spawned with weapons too heavy for them
        /// (For example in raids a child might otherwise spawn with an auto-shotty and promptly drop it the first time
        /// they fire it at you. Which would be silly.)
        /// </summary>
        [HarmonyPatch(typeof(PawnWeaponGenerator), "TryGenerateWeaponFor")]
        public static class PawnWeaponGenerator_TryGenerateWeaponFor_Patch {
            [HarmonyTranspiler]
            static IEnumerable<CodeInstruction> TryGenerateWeaponFor_Transpiler(IEnumerable<CodeInstruction> instructions) {
                List<CodeInstruction> ILs = instructions.ToList();

                int index = ILs.FindIndex(IL => IL.opcode == OpCodes.Ldfld && IL.operand.ToStringSafe().Contains("Pawn_EquipmentTracker")) - 1;

                MethodInfo giveChildWeapons = typeof(ChildrenUtility).GetMethod("FilterChildWeapons", AccessTools.all);
                var injection = new List<CodeInstruction> {
                new CodeInstruction(OpCodes.Ldarg_0),
                new CodeInstruction(OpCodes.Ldloc_2),
                new CodeInstruction(OpCodes.Callvirt, giveChildWeapons),
                new CodeInstruction(OpCodes.Stloc_2),
            };
                ILs.InsertRange(index, injection);

                foreach (CodeInstruction instruction in ILs) {
                    yield return instruction;
                }
            }
        }

        [HarmonyPatch(typeof(AttackTargetFinder), "BestShootTargetFromCurrentPosition")]
        public static class AttackTargetFinder_BestShootTargetFromCurrentPosition_Patch {
            [HarmonyPrefix]
            public static bool BestShootTargetFromCurrentPosition_Prefix(ref Predicate<Thing> validator) {
                if (validator == null) {
                    validator = new Predicate<Thing>(ShouldShootToddlers);
                }

                return true;
            }

            /// <summary>
            /// Predicate to decide whether a pawn is a valid target, specifically to filter toddlers
            /// </summary>
            /// <param name="t">A pawn, possibly humanlike</param>
            /// <returns>True if if the pawn is not a toddler or younger</returns>
            private static bool ShouldShootToddlers(Thing t) {
                Pawn pawn = t as Pawn;
                return !ChildrenUtility.RaceUsesChildren(pawn) || (ChildrenUtility.GetLifestageType(pawn) != null && ChildrenUtility.GetLifestageType(pawn) > LifestageType.Toddler);
            }
        }
    }
}
