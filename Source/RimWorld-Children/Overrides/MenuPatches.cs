﻿namespace RimWorldChildren {
    using HarmonyLib;
    using RimWorld;
    using RimWorldChildren.API;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using UnityEngine;
    using Verse;
    using Verse.AI;

    public class MenuPatches {

        [HarmonyPatch(typeof(FloatMenuMakerMap), "AddHumanlikeOrders")]
        public static class FloatMenuMakerMap_AddHumanLikeOrders_Patch {
            [HarmonyPostfix]
            public static void AddHumanlikeOrders_Postfix(Vector3 clickPos, Pawn pawn, List<FloatMenuOption> opts) {
                TargetingParameters targets = new TargetingParameters() { canTargetPawns = true, canTargetBuildings = false, mapObjectTargetsMustBeAutoAttackable = false, validator = targ => {
                    if (!targ.HasThing) {
                        return false;
                    }

                    Pawn thing = targ.Thing as Pawn;
                    return thing != null && thing != pawn && ChildrenUtility.CanBeAdopted(thing);
                }, };

                foreach (LocalTargetInfo localTargetInfo in GenUI.TargetsAt_NewTemp(clickPos, targets, true)) {
                    Pawn target = (Pawn)localTargetInfo.Thing;
                    string failMessage = null;
                    if (pawn.workSettings.GetPriority(CnpWorkTypeDefOf.Childcare) == 0) {
                        failMessage = !pawn.WorkTypeIsDisabled(CnpWorkTypeDefOf.Childcare) ? (!"CannotPrioritizeNotAssignedToWorkType".CanTranslate() ? "CannotPrioritizeWorkTypeDisabled".Translate(CnpWorkTypeDefOf.Childcare.pawnLabel) : "CannotPrioritizeNotAssignedToWorkType".Translate(CnpWorkTypeDefOf.Childcare.gerundLabel)) : "CannotPrioritizeWorkTypeDisabled".Translate(CnpWorkTypeDefOf.Childcare.gerundLabel);
                    } else if (!pawn.CanReach(target, PathEndMode.Touch, Danger.Deadly, false, TraverseMode.ByPawn)) {
                        failMessage = (target.Label + ": " + "NoPath".Translate()).CapitalizeFirst();
                    }

                    if (string.IsNullOrEmpty(failMessage) || (!pawn.WorkTypeIsDisabled(CnpWorkTypeDefOf.Childcare) && pawn.workSettings.GetPriority(CnpWorkTypeDefOf.Childcare) != 0 && ChildrenUtility.CanBeAdopted(target))) {
                        string message = "AdoptChildLabel".Translate(target.LabelCap, target);
                        bool angersFaction = false;

                        // if the child is in a non-enemy faction and there are other faction members on the map, they will be angry.
                        if (target.Faction != null && target.Faction != Faction.OfPlayer && (!target.Faction.def.hidden && !target.Faction.HostileTo(Faction.OfPlayer)) && !target.IsPrisonerOfColony && target.Map.mapPawns.PawnsInFaction(target.Faction).Count > 1) {
                            message += ": " + "AngersFaction".Translate().CapitalizeFirst();
                            angersFaction = true;
                        } 

                        opts.Add(FloatMenuUtility.DecoratePrioritizedTask(
                            new FloatMenuOption(message, () => {
                                Job job = JobMaker.MakeJob(CnpJobDefOf.AdoptBaby, target);
                                job.count = 1;
                                pawn.jobs.TryTakeOrderedJob(job, JobTag.Misc);
                                if (angersFaction) {
                                    Messages.Message("MessageCapturingWillAngerFaction".Translate(target.Named("PAWN")).AdjustedFor(target, "PAWN", true), target, MessageTypeDefOf.CautionInput, false);
                                }
                            }, MenuOptionPriority.RescueOrCapture, null, target, 0.0f, null, null), pawn, target, "ReservedBy"));
                    }
                    else {
                        opts.Add(FloatMenuUtility.DecoratePrioritizedTask(new FloatMenuOption(failMessage, null, MenuOptionPriority.Default, null, null, 0.0f, null, null), pawn, target, "ReservedBy"));
                    }
                }
            }
        }
    }
}
