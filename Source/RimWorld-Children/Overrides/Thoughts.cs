﻿namespace RimWorldChildren {
    using HarmonyLib;
    using RimWorld;
    using RimWorldChildren.API;
    using Verse;
    using Verse.AI;

    // Creates a blacklist of thoughts Toddlers cannot have
    [HarmonyPatch(typeof(ThoughtUtility), "CanGetThought_NewTemp")]
    public static class ThoughtUtility_CanGetThought_Patch{
        /// <summary>
        /// Prevent the acquisition of certain thoughts based on the life stage of this pawn
        /// </summary>
        [HarmonyPostfix]
        internal static void CanGetThought_Patch(ref Pawn pawn, ref ThoughtDef def, bool checkIfNullified, ref bool __result) {
            LifecycleComp comp = pawn.TryGetComp<LifecycleComp>();
            if (!comp?.CurrentLifestage?.cannotReceiveThoughts?.NullOrEmpty() ?? false) {
                __result = __result && !comp.CurrentLifestage.cannotReceiveThoughts.Contains(def);
            }
        }
    }

    [HarmonyPatch(typeof(ThoughtUtility), "GiveThoughtsForPawnExecuted")]
    public static class ThoughtUtility_GiveThoughtsForPawnExecuted_Patch {
        /// <summary>
        /// Override thoughts gained for executed children. Could be expanded to
        /// have special thoughts for organ harvesting, euthanization, etc
        /// </summary>
        [HarmonyPrefix]
        internal static bool GiveThoughtsForPawnExecuted_Prefix(Pawn victim, PawnExecutionKind kind) {
            LifecycleComp comp = victim.TryGetComp<LifecycleComp>();
            if (comp?.CurrentLifestage != null && comp.CurrentLifestage.lifestageType <= LifestageType.Child) {
                foreach (Pawn colonistsAndPrisoner in PawnsFinder.AllMapsCaravansAndTravelingTransportPods_Alive_FreeColonistsAndPrisoners) {
                    if (colonistsAndPrisoner.IsColonist && colonistsAndPrisoner.needs.mood != null) {
                        colonistsAndPrisoner.needs.mood.thoughts.memories.TryGainMemory(ThoughtMaker.MakeThought(CnpThoughtDefOf.ChildExecuted, 0), null);
                    }
                }

                return false;
            }

            return true;
        }
    }

    // Reroutes social fighting to account for children
    [HarmonyPatch(typeof(JobGiver_SocialFighting), "TryGiveJob")]
    public static class JobGiver_SocialFighting_TryGiveJob_Patch {
        [HarmonyPostfix]
        internal static void TryGiveJob_Postfix(ref Pawn pawn, ref Job __result){
            Pawn other = ((MentalState_SocialFighting)pawn.MentalState).otherPawn;
            if (__result != null) {
                // Make sure kids don't start social fights with adults
                if (ChildrenUtility.GetLifestageType(other) > LifestageType.Child && ChildrenUtility.GetLifestageType(pawn) <= LifestageType.Child) {
                    CLog.Message ("Child starting social fight with adult");

                    // Adult will "start" the fight, following the code below
                    other.interactions.StartSocialFight (pawn);
                    __result = null;
                }

                // Make sure adults don't start social fights with kids (unless psychopaths)
                if (ChildrenUtility.GetLifestageType(other) <= LifestageType.Child && ChildrenUtility.GetLifestageType(pawn) > LifestageType.Child && !pawn.story.traits.HasTrait(TraitDefOf.Psychopath)) {
                    // If the pawn is not in a bad mood or is kind, they'll just tell them off
                    if (pawn.story.traits.HasTrait (TraitDefOf.Kind) || pawn.needs.mood.CurInstantLevel > 0.45f || pawn.WorkTagIsDisabled(WorkTags.Violent)) {
                        JobDef chastise = DefDatabase<JobDef>.GetNamed("ScoldChild", true);
                        __result = new Job (chastise, other);
                    }

                    // Otherwise the adult will smack the child around
                    else if (other.health.summaryHealth.SummaryHealthPercent > 0.93f) {
                        JobDef paddlin = DefDatabase<JobDef>.GetNamed("DisciplineChild", true);
                        __result = new Job(paddlin, other);
                    }

                    pawn.MentalState.RecoverFromState();
                    __result = null;
                }
            }
        }
    }
}