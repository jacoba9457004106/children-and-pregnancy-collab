﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using System.Linq;
    using RimWorld;
    using Verse;

    /// <summary>
    /// Mod settings panel for CNP
    /// </summary>
    public class CnpSettings : ModSettings {
        public int maxTraits = 3;
        public bool debugLogging = false;

        public int populationCap = 0;
        public float cryVolume = .75f;

        public bool acceleratedGrowthEnabled = false;
        public int acceleratedGrowthEndAge = 12;
        public int babyAccelerationFactor = 2;
        public int toddlerAccelerationFactor = 2;
        public int childAccelerationFactor = 2;

        public int randTraits = 0;

        public Dictionary<string, RacialSettings> racialSettings;

        public RacialSettings RacialSettings(string raceDefName) {
            if (racialSettings[raceDefName] == null) {
                // The race wasn't loaded at launch, indicating some sort of problem with the config for this race
                // To prevent problems, we will validate the race is real, then add it to the config at run time
                if (DefDatabase<ThingDef>.AllDefs.FirstOrFallback(def => def.defName == raceDefName, null) != null) {
                    racialSettings.Add(raceDefName, new RacialSettings() { pregnancyEnabled = false, lifeCycleEnabled = false });
                    CLog.Warning("Adding a race (" + raceDefName + ") to CNP post-launch. This likely indicates a serious configuration issue. This may be resolved after saving and loading.");
                }
            }

            return racialSettings[raceDefName];
        }

        public override void ExposeData() {
            base.ExposeData();
            Scribe_Values.Look(ref randTraits, "randTraits");
            Scribe_Values.Look(ref maxTraits, "maxTraits");
            Scribe_Values.Look(ref debugLogging, "debugLogging");
            Scribe_Values.Look(ref populationCap, "populationCap");
            Scribe_Values.Look(ref cryVolume, "cryVolume");
            Scribe_Values.Look(ref acceleratedGrowthEnabled, "acceleratedGrowthEnabled");
            Scribe_Values.Look(ref acceleratedGrowthEndAge, "acceleratedGrowthEndAge");
            Scribe_Values.Look(ref babyAccelerationFactor, "babyAccelerationFactor");
            Scribe_Values.Look(ref toddlerAccelerationFactor, "toddlerAccelerationFactor");
            Scribe_Values.Look(ref childAccelerationFactor, "childAccelerationFactor");
            Scribe_Collections.Look(ref racialSettings, "racialSettings", LookMode.Value, LookMode.Deep);
        }

        public void Initialize() {
            if (racialSettings == null) {
                racialSettings = new Dictionary<string, RacialSettings>();
            } else {
                // If a player removes a race from their load order, we must remove it from our saved settings to prevent type load exceptions
                racialSettings.RemoveAll(race => DefDatabase<ThingDef>.AllDefs.FirstOrFallback(def => def.defName == race.Key, null) == null);
            }

            foreach (ThingDef thingDef in DefDatabase<ThingDef>.AllDefs) {
                if (thingDef.race != null && !racialSettings.ContainsKey(thingDef.defName)) {
                    // By default, enable races that kindof look like humans
                    bool enable = thingDef.race != null && thingDef.race.Humanlike && thingDef.race.lifeStageAges?.Count == 5;

                    // Ensure that the race has no custom lifestages, we can't support them by default.
                    enable = enable && thingDef.race.lifeStageAges.Select(ls => ls.def).ToList().Except(LifeStageDefOf.AsList()).Count() == 0;
                    racialSettings.Add(thingDef.defName, new RacialSettings() { pregnancyEnabled = enable, lifeCycleEnabled = enable });
                }

                if (thingDef.race != null && thingDef.race.Humanlike) {
                    // TODO: Move this to an xpath patch
                    thingDef.comps.Add(new CompProperties_CarryChild());
                }

                // Probably need to find a better way to add this comp dynamically
                if (thingDef.race != null && racialSettings.ContainsKey(thingDef.defName) && racialSettings[thingDef.defName].lifeCycleEnabled) {
                    ValidateRaceDef(thingDef);
                    thingDef.comps.Add(new CompProperties_LifeCycle());
                }
            }
        }

        /// <summary>
        /// This method will look at an enabled race and the RacialLifestageDef it resolves to determine
        /// if the resolved def is properly defined for the race. An error will be logged if the def is invalid.
        /// </summary>
        /// <param name="thing">A ThingDef representing a pawn race</param>
        /// <returns> true if there are no errors identified in the def</returns>
        private bool ValidateRaceDef(ThingDef thing) {
            RacialLifestageDef lifecycleDef;
            lifecycleDef = DefDatabase<RacialLifestageDef>.GetNamed(thing.defName + "_Lifestages", false);
            List<LifeStageDef> raceStages = thing.race.lifeStageAges.Select(ls => ls.def).ToList();
            if (lifecycleDef == null) {
                lifecycleDef = DefDatabase<RacialLifestageDef>.GetNamed("Default_Lifestages", true);
            }

            List<LifeStageDef> defStages = lifecycleDef.lifeStages.Select(ls => ls.lifeStageDef).ToList();

            if (raceStages.Except(defStages).Count() > 0) {
                Log.Error("Race " + thing.defName + " contains lifestages that are not present in the resolved RacialLifestageDef: " + lifecycleDef.defName
                            + " this race will need a patch before loading properly. Otherwise, disable lifeCycle in the settings.");
                return false;
            }

            for (int i = 0, j = 0; i < defStages.Count(); ++i) {
                // defStages may have duplicates, so lets only increment when we see a new lifestage
                if (defStages[i] != raceStages[j]) {
                    ++j;
                }

                if (defStages[i] != raceStages[j]) {
                    Log.Error("Race " + thing.defName + " contains lifestages that are not in same order as the resolved RacialLifestageDef: " + lifecycleDef.defName
                                + " this race will need a patch before loading properly. Otherwise, disable lifeCycle in the settings.");
                    return false;
                }
            }

            for (int i = 1; i < defStages.Count(); ++i) {
                // Ensure each defStage is incrementing the age, as we assume these stages are in ascending order
                if (lifecycleDef.lifeStages[i].minAge > -1 && lifecycleDef.lifeStages[i - 1].minAge > lifecycleDef.lifeStages[i].minAge) {
                    Log.Error("Race " + thing.defName + " contains lifestages with age requirements are not in ascending order in the RacialLifestageDef: " + lifecycleDef.defName
                                + " this race will need a patch before loading properly. Otherwise, disable lifeCycle in the settings.");
                    return false;
                }
            }

            return true;
        }
    }
}
